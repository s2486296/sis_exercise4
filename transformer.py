from rdflib import Graph, Namespace, URIRef, Literal
from rdflib.namespace import RDF, RDFS
import pandas as pd
import sys

# Define file paths for input CSV and output Turtle file
input_file_path = sys.argv[1]
output_file_path = "graph.ttl"

# Load dataset
data = pd.read_csv(input_file_path, index_col=0, parse_dates=True, nrows=200)
data = data.drop(['cet_cest_timestamp', 'interpolated'], axis=1)

# Define namespaces
SAREF = Namespace("https://w3id.org/saref#")
XSD = Namespace("http://www.w3.org/2001/XMLSchema#")
EX = Namespace("http://example.org/Koen/")


def split_columnName(name):
    splits = name.split('_')
    building_Name = '_'.join(splits[:3])
    device = '_'.join(splits[3:])
    return building_Name, device

# Create RDF graph
g = Graph()

#Bind the prefixes
g.bind("saref", SAREF)
g.bind("xsd", XSD)
g.bind("ex", EX)

URI_Unit = URIRef(EX['Units/kWh'])

# Loop over every row in the dataset
for timestamp, row in data.iterrows():
    ##Create a URI for every timestamp
    URI_Timestamp = URIRef(EX[f'timestamps/{timestamp.isoformat()}'])
    
    for column, value in row.items():
        
        if pd.notna(value):
            #retrive the name of the building and device
            building_name, device = split_columnName(column)

            #Create URI's for the device, building and measurement
            URI_Device = URIRef(EX[f'Devices/{building_name}/{device}'])
            URI_Building = URIRef(EX[f'Buildings/{building_name}'])
            URI_Measurement = URIRef(EX[f'Measurements/{building_name}/{device}/{timestamp.isoformat()}'])


            # Add triples
            g.add((URI_Measurement, RDF.type, SAREF.Measurement))
            g.add((URI_Measurement, SAREF.hasTimestamp, Literal(timestamp.isoformat(), datatype=XSD.dateTime)))
            g.add((URI_Measurement, SAREF.isMeasuredIn, URI_Unit))
            g.add((URI_Measurement, SAREF.hasValue, Literal(float(value), datatype=XSD.float)))
            g.add((URI_Measurement, EX['is_measurement_Of'], URI_Device))
            
            g.add((URI_Device, RDF.type, SAREF.Device))
            g.add((URI_Device, EX['has_measurement'], URI_Measurement))
            g.add((URI_Device, SAREF.isContainedIn, URI_Building))
            
            g.add((URI_Building, RDF.type, SAREF.Building))
            g.add((URI_Building, SAREF.Contains, URI_Device))
            
# Serialize the graph in Turtle format
turtle_data = g.serialize(format='turtle')

# Write the Turtle data to a file
with open(output_file_path, "w", encoding="utf-8") as output_file:
    output_file.write(turtle_data)  # Save the Turtle data to a file

print(f"RDF graph has successfully been serialized and saved as {output_file_path}") 
